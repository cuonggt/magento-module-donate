<?php

namespace Cuonggt\Donate\Block;

use Cuonggt\Donate\Helper\Data;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Helper\Image;
use Magento\Checkout\Model\Cart;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

class Donate extends Template
{
    protected $dataHelper;

    protected $productRepository;

    protected $imageHelper;

    protected $cart;

	public function __construct(
        Context $context,
        Data $dataHelper,
        ProductRepositoryInterface $productRepository,
        Image $imageHelper,
        Cart $cart
    )
	{
        $this->dataHelper = $dataHelper;
        $this->productRepository = $productRepository;
        $this->imageHelper = $imageHelper;
        $this->cart = $cart;

		parent::__construct($context);
	}

    public function canShowBlock()
    {
        return $this->dataHelper->isModuleEnabled() && $this->configProduct();
    }

    public function configProduct()
    {
        if (! $this->dataHelper->sku()) {
            return null;
        }

        return $this->productRepository->get($this->dataHelper->sku());
    }

    public function getProductImageUrl()
    {
        return $this->imageHelper
            ->init($this->configProduct(), 'product_base_image')
            ->constrainOnly(false)
            ->keepAspectRatio(true)
            ->keepFrame(false)
            ->getUrl();
    }

    public function configTitle()
    {
        return $this->dataHelper->title() ?? $this->configProduct()->getName();
    }

    public function configDescription()
    {
        return $this->dataHelper->description();
    }

    public function configAmount()
    {
        return $this->dataHelper->amount();
    }

    public function getDonateFormAction()
    {
        return '/donate/index';
    }

    public function getCancelFormAction()
    {
        return '/donate/index/cancel';
    }

    public function donated()
    {
        foreach ($this->cart->getItems() as $item) {
            if ($item->getProductId() == $this->configProduct()->getId()) {
                return true;
            }
        }

        return false;
    }
}
