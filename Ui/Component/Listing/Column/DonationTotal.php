<?php

namespace Cuonggt\Donate\Ui\Component\Listing\Column;

use Cuonggt\Donate\Helper\Data;
use Magento\Directory\Model\Currency;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Ui\Component\Listing\Columns\Column;

class DonationTotal extends Column
{
    protected $orderRepository;

    protected $dataHelper;

    protected $priceFormatter;

    private $currency;

    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        OrderRepositoryInterface $orderRepository,
        Data $dataHelper,
        PriceCurrencyInterface $priceFormatter,
        array $components = [],
        array $data = [],
        Currency $currency = null
    )
    {
        $this->orderRepository = $orderRepository;
        $this->dataHelper = $dataHelper;
        $this->priceFormatter = $priceFormatter;
        $this->currency = $currency ?: ObjectManager::getInstance()->create(Currency::class);

        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $order = $this->orderRepository->get($item['entity_id']);

                $donationTotal = 0;

                foreach ($order->getItems() as $orderItem) {
                    if ($orderItem->getSku() == $this->dataHelper->sku()) {
                        $donationTotal = $orderItem->getRowTotal();
                        break;
                    }
                }

                $currencyCode = isset($item['base_currency_code']) ? $item['base_currency_code'] : null;
                $basePurchaseCurrency = $this->currency->load($currencyCode);
                $item[$this->getData('name')] = $basePurchaseCurrency->format($donationTotal, [], false);
            }
        }

        return $dataSource;
    }
}
