<?php

namespace Cuonggt\Donate\Ui\Component\Listing\Column;

use Cuonggt\Donate\Helper\Data;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Ui\Component\Listing\Columns\Column;

class IsDonated extends Column
{
    protected $orderRepository;

    protected $dataHelper;

    protected $criteria;

    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        OrderRepositoryInterface $orderRepository,
        Data $dataHelper,
        array $components = [],
        array $data = [])
    {
        $this->orderRepository = $orderRepository;
        $this->dataHelper = $dataHelper;

        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $order = $this->orderRepository->get($item['entity_id']);

                $item[$this->getData('name')] = 0;

                foreach ($order->getItems() as $orderItem) {
                    if ($orderItem->getSku() == $this->dataHelper->sku()) {
                        $item[$this->getData('name')] = 1;
                        break;
                    }
                }
            }
        }

        return $dataSource;
    }
}
