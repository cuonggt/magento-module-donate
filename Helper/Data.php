<?php

namespace Cuonggt\Donate\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{
    const MODULE_ENABLE = 'donate/general/enable';
    const MODULE_SKU = 'donate/general/sku';
    const MODULE_TITLE = 'donate/general/title';
    const MODULE_DESCRIPTION = 'donate/general/description';
    const MODULE_AMOUNT = 'donate/general/amount';

    public function getConfig($field)
    {
        return $this->scopeConfig->getValue(
            $field,
            ScopeInterface::SCOPE_STORE
        );
    }

    public function isModuleEnabled()
    {
        return (bool) $this->getConfig(self::MODULE_ENABLE);
    }

    public function sku()
    {
        return $this->getConfig(self::MODULE_SKU);
    }

    public function title()
    {
        return $this->getConfig(self::MODULE_TITLE);
    }

    public function description()
    {
        return $this->getConfig(self::MODULE_DESCRIPTION);
    }

    public function amount()
    {
        return (int) $this->getConfig(self::MODULE_AMOUNT);
    }
}
