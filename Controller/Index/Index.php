<?php

namespace Cuonggt\Donate\Controller\Index;

use Cuonggt\Donate\Helper\Data;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Checkout\Model\Cart;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Data\Form\FormKey\Validator;

class Index extends Action
{
    protected $dataHelper;

    protected $formKeyValidator;

    protected $productRepository;

    protected $cart;

    public function __construct(
        Context $context,
        Data $dataHelper,
        Validator $formKeyValidator,
        ProductRepositoryInterface $productRepository,
        Cart $cart
    )
    {
        $this->dataHelper = $dataHelper;
        $this->formKeyValidator = $formKeyValidator;
        $this->productRepository = $productRepository;
        $this->cart = $cart;

        parent::__construct($context);
    }

    public function execute()
    {
        if (! $this->formKeyValidator->validate($this->getRequest())) {
            $this->messageManager->addErrorMessage(
                __('Your session has expired')
            );

            return $this->resultRedirectFactory->create()->setPath('*/*/');
        }

        $product = $this->productRepository->get($this->dataHelper->sku());

        if (! $product) {
            return $this->resultRedirectFactory->create()->setPath('checkout/cart');
        }

        try {
            $params = [
                'qty' => $this->getRequest()->getParams()['amount'] ?? 1,
            ];

            $this->cart->addProduct($product, $params);
            $this->cart->save();
        } catch (\Exception $e) {
            $this->messageManager->addExceptionMessage(
                $e,
                __('We can\'t add the donation to your shopping cart right now.')
            );
            $this->_objectManager->get(\Psr\Log\LoggerInterface::class)->critical($e);
        }

        return $this->resultRedirectFactory->create()->setPath('checkout/cart');
    }
}
