<?php

namespace Cuonggt\Donate\Controller\Index;

use Cuonggt\Donate\Helper\Data;
use Magento\Checkout\Model\Cart;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Data\Form\FormKey\Validator;

class Cancel extends Action
{
    protected $dataHelper;

    protected $formKeyValidator;

    protected $cart;


    public function __construct(
        Context $context,
        Data $dataHelper,
        Validator $formKeyValidator,
        Cart $cart
    )
    {
        $this->dataHelper = $dataHelper;
        $this->formKeyValidator = $formKeyValidator;
        $this->cart = $cart;

        parent::__construct($context);
    }

    public function execute()
    {
        if (!$this->formKeyValidator->validate($this->getRequest())) {
            return $this->resultRedirectFactory->create()->setPath('*/*/');
        }

        foreach ($this->cart->getItems() as $cartItem) {
            if ($cartItem->getProduct()->getSku() != $this->dataHelper->sku()) {
                continue;
            }

            try {
                $this->cart->removeItem($cartItem->getItemId());
                $this->cart->getQuote()->setTotalsCollectedFlag(false);
                $this->cart->save();
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage(__('We can\'t cancel the donation.'));
                $this->_objectManager->get(\Psr\Log\LoggerInterface::class)->critical($e);
            }
        }

	    return $this->resultRedirectFactory->create()->setPath('checkout/cart');
    }
}
